package de.favo.radio.callbacks;

import android.support.v4.media.MediaBrowserCompat;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MediaBrowserCallback extends MediaBrowserCompat.ConnectionCallback {

    private ConnectionCallbackInternal connectionCallbackInternal;

    public MediaBrowserCallback(ConnectionCallbackInternal callback) {
        super();
        this.connectionCallbackInternal = callback;
    }

    /**
     * Invoked after {@link MediaBrowserCompat#connect()} when the request has successfully
     * completed.
     */
    public void onConnected() {
        connectionCallbackInternal.onConnected();
    }

    /**
     * Invoked when the client is disconnected from the media browser.
     */
    public void onConnectionSuspended() {
        connectionCallbackInternal.onConnectionSuspended();
    }

    /**
     * Invoked when the connection to the media browser failed.
     */
    public void onConnectionFailed() {
        connectionCallbackInternal.onConnectionFailed();
    }


    public interface ConnectionCallbackInternal {
        void onConnected();
        void onConnectionSuspended();
        void onConnectionFailed();
    }
}
