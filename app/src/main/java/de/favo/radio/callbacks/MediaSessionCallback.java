package de.favo.radio.callbacks;

import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MediaSessionCallback extends MediaSessionCompat.Callback {

    private Callback callback;

    public MediaSessionCallback(Callback callback){
        super();
        this.callback = callback;
    }

    @Override
    public void onPause() {
        super.onPause();
        callback.onPause();
    }

    @Override
    public void onPlay() {
        super.onPlay();
        callback.onPlay();
    }

    @Override
    public void onStop() {
        super.onStop();
        callback.onStop();
    }

    @Override
    public void onSkipToNext() {
        super.onSkipToNext();
        callback.onSkipToNext();
    }

    @Override
    public void onSkipToPrevious() {
        super.onSkipToPrevious();
        callback.onSkipToPrevious();
    }

    @Override
    public void onPlayFromSearch(String query, Bundle extras) {
        super.onPlayFromSearch(query, extras);
        callback.onPlayFromSearch(query,extras);
    }

    @Override
    public void onPlayFromMediaId(String mediaId, Bundle extras) {
        super.onPlayFromMediaId(mediaId, extras);
        callback.onPlayFromMediaId(mediaId, extras);
    }

    public interface Callback{
        void onPlay();
        void onPause();
        void onSkipToNext();
        void onSkipToPrevious();
        void onStop();
        void onPlayFromMediaId(String mediaId, Bundle extras);
        void onPlayFromSearch(String search, Bundle bundle);

    }
}
