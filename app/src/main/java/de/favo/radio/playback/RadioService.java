package de.favo.radio.playback;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaBrowserServiceCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import java.util.List;
import de.favo.radio.R;
import de.favo.radio.callbacks.MediaSessionCallback;
import de.favo.radio.management.PlaybackManager;
import de.favo.radio.tools.BroadcastCallback;
import de.favo.radio.tools.PowerLock;
import de.favo.radio.tools.Utils;
import de.favo.radio.tools.radio.RadioStation;
import de.favo.radio.tools.radio.RadioStationListPlayingManagement;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class RadioService extends MediaBrowserServiceCompat implements AudioPlayer.Callback,
        MediaSessionCallback.Callback, BroadcastCallback.Callback {

    private AudioPlayer mAudioPlayer;
    private MediaSessionCompat mMediaSessionCompat;
    private PowerLock mPowerLock;
    private RadioStationListPlayingManagement stationManagement;
    private PlaybackManager mPlaybackManager;
    public MediaSessionCallback mediaSessionCallback = new MediaSessionCallback(this);
    private BroadcastReceiver noisyReceiver;
    private long playingTime = 0L;

    @Override
    public void onCreate() {
        super.onCreate();
        mAudioPlayer = new AudioPlayer(this,this);
        mPowerLock = new PowerLock(this);
        mPowerLock.acquire();
        stationManagement = new RadioStationListPlayingManagement(this);
        ComponentName mediaButtonReceiver = new ComponentName(getApplicationContext(), MediaButtonReceiver.class);
        mMediaSessionCompat = new MediaSessionCompat(getApplicationContext(), "Tag", mediaButtonReceiver, null);
        mMediaSessionCompat.setCallback(mediaSessionCallback);
        mMediaSessionCompat.setFlags( MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS );
        Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        mediaButtonIntent.setClass(this, MediaButtonReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0);
        mMediaSessionCompat.setMediaButtonReceiver(pendingIntent);

        setSessionToken(mMediaSessionCompat.getSessionToken());
        noisyReceiver = new BroadcastCallback(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(noisyReceiver, filter);
        mPlaybackManager = new PlaybackManager(this);
        setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED);
        mMediaSessionCompat.setActive(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(noisyReceiver);
        mAudioPlayer.stop(true);
        mPowerLock.release();
        mAudioPlayer.close();
        NotificationManagerCompat.from(this).cancel(Utils.PLAYER_CHANNEL_ID.hashCode());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MediaButtonReceiver.handleIntent(mMediaSessionCompat, intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onPlay() {
        mMediaSessionCompat.setActive(true);
        setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING);
        mAudioPlayer.play();
    }

    @Override
    public void onPause() {
        setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED);
        mAudioPlayer.pause();
    }

    @Override
    public void onSkipToNext() {
        stationManagement.reload();
        RadioStation station = stationManagement.nextStation();
        setFirstMetaData(station);
        setMediaPlaybackState(PlaybackStateCompat.STATE_BUFFERING);
        mAudioPlayer.playStation(station);
    }

    @Override
    public void onSkipToPrevious() {
        stationManagement.reload();
        RadioStation station = stationManagement.previousStation();
        setFirstMetaData(station);
        setMediaPlaybackState(PlaybackStateCompat.STATE_BUFFERING);
        mAudioPlayer.playStation(station);
    }

    @Override
    public void onStop() {
        setMediaPlaybackState(PlaybackStateCompat.STATE_STOPPED);
        mMediaSessionCompat.setActive(false);
        mAudioPlayer.stop(true);
        stopSelf();
    }

    @Override
    public void onPlayFromMediaId(String mediaId, Bundle extras) {
        onPlayFromSearch(mediaId,extras);
    }

    @Override
    public void onPlayFromSearch(String search, Bundle bundle) {
        playingTime = System.currentTimeMillis();
        stationManagement.reload();
        RadioStation result = stationManagement.getStationByNameForPlaying(search);
        if (result == null){
            return;
        }
        setFirstMetaData(result);
        setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING);
        mAudioPlayer.playStation(result);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction()) && mAudioPlayer != null && mAudioPlayer.isPlaying() ) {
            mAudioPlayer.pause();
        }
        if (Intent.ACTION_SCREEN_ON.equals(intent.getAction()) && mAudioPlayer != null){
            mAudioPlayer.resumeTexts();
        }
        if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction()) && mAudioPlayer != null){
            mAudioPlayer.pauseTexts();
        }
    }

    @Override
    public void metadataChanged(String text) {
        MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();

        if (text.trim().isEmpty())
            text = mAudioPlayer.getStation().getUrl();

        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE,text);
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE,text);
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, mAudioPlayer.getStation().getName());
        metadataBuilder.putString("color", mAudioPlayer.getStation().getColor());
        MediaMetadataCompat metadata = metadataBuilder.build();
        mMediaSessionCompat.setMetadata(metadata);
        mMediaSessionCompat.setExtras(mAudioPlayer.getExtraInformation());
        mPlaybackManager.update(metadata,
                mMediaSessionCompat.getController().getPlaybackState(),
                mAudioPlayer.isBuffering(), getSessionToken());

    }

    @Override
    public void onBufferingStateChanged(boolean isBuffering) {
        if (mMediaSessionCompat.getController().getPlaybackState().getState() ==
                PlaybackStateCompat.STATE_PLAYING ||
                mMediaSessionCompat.getController().getPlaybackState().getState() ==
                PlaybackStateCompat.STATE_BUFFERING)
            setMediaPlaybackState(isBuffering ? PlaybackStateCompat.STATE_BUFFERING :
                    PlaybackStateCompat.STATE_PLAYING);
    }

    @Override
    public void onSourceNotReachable() {
        mAudioPlayer.pause();
        PlaybackStateCompat.Builder playbackstateBuilder = new PlaybackStateCompat.Builder();
        playbackstateBuilder.setErrorMessage(PlaybackStateCompat.ERROR_CODE_NOT_AVAILABLE_IN_REGION,getString(R.string.text_error_invalid_url));
        playbackstateBuilder.setState(PlaybackStateCompat.STATE_ERROR, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 0);
        mMediaSessionCompat.setPlaybackState(playbackstateBuilder.build());
    }

    @Override
    public void onNoSourceSelected() {
        PlaybackStateCompat.Builder playbackstateBuilder = new PlaybackStateCompat.Builder();
        playbackstateBuilder.setErrorMessage(PlaybackStateCompat.ERROR_CODE_END_OF_QUEUE,getString(R.string.text_error_no_station));
        playbackstateBuilder.setState(PlaybackStateCompat.STATE_ERROR, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN,
                0);
        mMediaSessionCompat.setPlaybackState(playbackstateBuilder.build());
    }

    private void setMediaPlaybackState(int state) {
        PlaybackStateCompat.Builder playbackstateBuilder = new PlaybackStateCompat.Builder();

        if (state == PlaybackStateCompat.STATE_PLAYING) {
            playbackstateBuilder.setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE |
                            PlaybackStateCompat.ACTION_SKIP_TO_NEXT|
                            PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
                            PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH |
                            PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID |
                            PlaybackStateCompat.ACTION_PAUSE);
        } else {
            playbackstateBuilder.setActions(
                    PlaybackStateCompat.ACTION_PLAY_PAUSE |
                            PlaybackStateCompat.ACTION_SKIP_TO_NEXT|
                            PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
                            PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH |
                            PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID |
                            PlaybackStateCompat.ACTION_PLAY);
        }
        playbackstateBuilder.setState(state, playingTime, 1f);
        PlaybackStateCompat stateCompat = playbackstateBuilder.build();
        mMediaSessionCompat.setPlaybackState(stateCompat);

        mPlaybackManager.update(mMediaSessionCompat.getController().getMetadata(),stateCompat,
                mAudioPlayer.isBuffering(), getSessionToken());
    }

    public void setFirstMetaData(RadioStation firstMetaData) {
        MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE,firstMetaData.getUrl());
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE,firstMetaData.getName());
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ARTIST,firstMetaData.getName());
        metadataBuilder.putString("color",firstMetaData.getColor());
        mMediaSessionCompat.setMetadata(metadataBuilder.build());
    }

    @Nullable
    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName,
                                 int clientUid, @Nullable Bundle rootHints) {
        return new BrowserRoot(getString(R.string.app_name), null);
    }

    @Override
    public void onLoadChildren(@NonNull String parentId,
                               @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {
        result.sendResult(null);
    }

}