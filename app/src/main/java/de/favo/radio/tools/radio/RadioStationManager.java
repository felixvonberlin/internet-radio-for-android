package de.favo.radio.tools.radio;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Vector;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class RadioStationManager {

    private Vector<RadioStation> mRadioStationsVector;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mSharedPreferencesEditor;

    @SuppressLint("CommitPrefEdits")
    public RadioStationManager(Context context){
        mRadioStationsVector = new Vector<>(15);

        mSharedPreferences = context.getSharedPreferences("RadioStations", Context.MODE_PRIVATE);
        mSharedPreferencesEditor = mSharedPreferences.edit();
        reloadStations();
    }

    public void reloadStations(){
        int stations = mSharedPreferences.getInt("number_of_radio_stations", 0);
        String name, color, url;
        int position;

        if (!mRadioStationsVector.isEmpty())
            mRadioStationsVector.clear();

        for (int i = 0; i < stations; i++){
            name = mSharedPreferences.getString("radio_station_name_"+i,"");
            color = mSharedPreferences.getString("radio_station_color_"+i, "#cccccc");
            url = mSharedPreferences.getString("radio_station_url_"+i,"");
            mRadioStationsVector.add(new RadioStation(name,url,color));
        }
    }

    public void addStation(RadioStation radioStation){
        mRadioStationsVector.add(0,radioStation);
        saveState();
    }

    public void addStation(RadioStation radioStation, int index){
        mRadioStationsVector.add(index, radioStation);
        saveState();
    }

    public void moveStationUp(String station){
        int pos = findRadioStationPositionByName(station);
        if (pos != -1 && pos > 0){
            RadioStation
                    r1 = mRadioStationsVector.elementAt(pos),
                    r2 = mRadioStationsVector.elementAt(pos - 1);
            mRadioStationsVector.setElementAt(r1,pos-1);
            mRadioStationsVector.setElementAt(r2,pos);
            saveState();
        }
    }
    public void moveStationDown(String station){
        int pos = findRadioStationPositionByName(station);
        if (pos != -1 && pos < mRadioStationsVector.size()){
            RadioStation
                    r1 = mRadioStationsVector.elementAt(pos),
                    r2 = mRadioStationsVector.elementAt(pos + 1);
            mRadioStationsVector.setElementAt(r1,pos+1);
            mRadioStationsVector.setElementAt(r2,pos);
            saveState();
        }
    }

    public boolean isNameAlreadyExisting(String stationName){
        for (RadioStation station : mRadioStationsVector)
            if (station.getName().equals(stationName))
                return true;
        return false;
    }

    public Vector<RadioStation> getRadioStations() {
        return mRadioStationsVector;
    }

    public int findRadioStationPositionByName(String name){
        for (int x = 0; x < mRadioStationsVector.size(); x++) {
            if (mRadioStationsVector.get(x).getName().equals(name))
                return x;
        }
        return -1;
    }

    public RadioStation findByPosition(int pos){
        if (mRadioStationsVector.size() > pos)
            return mRadioStationsVector.get(pos);
        return null;
    }

    public RadioStation findRadioStationByName(String name){
        int pos = findRadioStationPositionByName(name);
        return pos >= 0 && pos < mRadioStationsVector.size() ? mRadioStationsVector.get(pos) : null;
    }

    public int getPositionOf(RadioStation s){
        return mRadioStationsVector.indexOf(s);
    }

    public void deleteStationByPosition(int pos){
        mRadioStationsVector.remove(pos);
        saveState();
    }
    public void deleteStationStation(RadioStation radioStation){
        mRadioStationsVector.remove(radioStation);
        saveState();
    }

    private void saveState(){
        RadioStation r;
        mSharedPreferencesEditor.putInt("number_of_radio_stations", mRadioStationsVector.size());
        for (int d = 0; d < mRadioStationsVector.size(); d++){
            r = mRadioStationsVector.get(d);
            mSharedPreferencesEditor.putString("radio_station_name_"+d, r.getName());
            mSharedPreferencesEditor.putString("radio_station_color_"+d, r.getColor());
            mSharedPreferencesEditor.putString("radio_station_url_"+d, r.getUrl());
        }
        mSharedPreferencesEditor.commit();
        mSharedPreferencesEditor = mSharedPreferences.edit();
    }
}
