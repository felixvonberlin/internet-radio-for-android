package de.favo.radio.tools;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MetaDataRetriever extends Thread{
    private Callback mCallback;
    private URL mURL;
    private boolean pause = false;
    private static long loopUpWait = 2000L;

    public MetaDataRetriever(Callback callback, String url) {
        this.mCallback = callback;
        try {
            this.mURL = new URL(url);
        } catch (MalformedURLException e) {
            callback.onUrlError();
        }
    }

    public void run(){
        IcyStreamMeta data = new IcyStreamMeta();
        String text;
        while (isAlive()){
            while (!pause) {
                try {
                    data.setStreamUrl(mURL);
                    text = data.getStreamTitle();
                    mCallback.onStreamTitleChanged(fixEncoding(text));
                } catch (Throwable e) {
                    mCallback.onTextError();
                }
                try{
                    sleep(loopUpWait);
                } catch (InterruptedException ignored) { }
            }
            if (pause)
                try{
                    sleep(loopUpWait);
                } catch (InterruptedException ignored) { }
        }
    }

    private static String fixEncoding(String latin1) {
        try {
            byte[] bytes = latin1.getBytes("ISO-8859-1");
            if (!validUTF8(bytes))
                return latin1;
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // Impossible, throw unchecked
            throw new IllegalStateException("No Latin1 or UTF-8: " + e.getMessage());
        }

    }

    private static boolean validUTF8(byte[] input) {
        int i = 0;
        // Check for BOM
        if (input.length >= 3 && (input[0] & 0xFF) == 0xEF
                && (input[1] & 0xFF) == 0xBB & (input[2] & 0xFF) == 0xBF) {
            i = 3;
        }

        int end;
        for (int j = input.length; i < j; ++i) {
            int octet = input[i];
            if ((octet & 0x80) == 0) {
                continue; // ASCII
            }

            // Check for UTF-8 leading byte
            if ((octet & 0xE0) == 0xC0) {
                end = i + 1;
            } else if ((octet & 0xF0) == 0xE0) {
                end = i + 2;
            } else if ((octet & 0xF8) == 0xF0) {
                end = i + 3;
            } else {
                // Java only supports BMP so 3 is max
                return false;
            }

            while (i < end) {
                i++;
                octet = input[i];
                if ((octet & 0xC0) != 0x80) {
                    // Not a valid trailing byte
                    return false;
                }
            }
        }
        return true;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    public interface Callback{
        void onStreamTitleChanged(String name);
        void onTextError();
        void onUrlError();
    }
}