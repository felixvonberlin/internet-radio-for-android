package de.favo.radio.tools;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import java.util.Vector;
import de.favo.radio.R;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

public class Utils {
    public static final String PLAYER_CHANNEL_ID = "de.favo.radiostreamplayer.PLAYER_CHANNEL";

    /**
     * Returns darker version of specified <code>color</code>.
     */
    public static int darker (int color, float factor) {
        int a = Color.alpha( color );
        int r = Color.red( color );
        int g = Color.green( color );
        int b = Color.blue( color );

        return Color.argb( a,
                Math.max( (int)(r * factor), 0 ),
                Math.max( (int)(g * factor), 0 ),
                Math.max( (int)(b * factor), 0 ) );
    }

    public static void setUpNotificationChannels(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = c.getString(R.string.notification_channels_player_channel);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel mChannel = new NotificationChannel(PLAYER_CHANNEL_ID,
                    name, importance);
            NotificationManager notificationManager = (NotificationManager) c.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }
    }

    public static boolean isColorDark(int color) {
        double darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;
        return darkness > 0.4;
    }

    public static String getFileEnding(String uri){
        int pos = uri.lastIndexOf('.');
        if (pos == -1)
            return "none";
        else
            return uri.substring(pos);
    }

    public static boolean contains(Object[] array, Object entry){
        for (Object o : array)
            if (o.equals(entry))
                return true;
        return false;
    }

    public static int findLineWhichStartsWithIgnoreCase(Vector<String> vector, String start){
        for (int i = 0; i < vector.size(); i++) {
            if (vector.get(i).toLowerCase().startsWith(start.toLowerCase()))
                return i;
        }
        return -1;
    }

    public static Drawable colorizeDrawable(Drawable drawable, int color){
        if (drawable == null)
            return null;
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable.mutate(), color);
        return drawable;
    }
}
