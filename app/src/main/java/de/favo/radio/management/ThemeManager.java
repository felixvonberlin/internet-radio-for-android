package de.favo.radio.management;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import de.favo.radio.R;
import de.favo.radio.tools.MarqueeToolbar;
import de.favo.radio.ui.RadioActivity;
import de.favo.radio.tools.Utils;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class ThemeManager {

    private static final int color = Color.WHITE;

    private int mActualColor;
    private RadioActivity mRadioActivity;
    private boolean lightTheme = true;

    public ThemeManager(RadioActivity activity){
        mRadioActivity = activity;
        mActualColor = color;
    }

    public int getColor() {
        return mActualColor;
    }

    public Drawable getDrawable(int res){
        return Utils.colorizeDrawable(mRadioActivity.getDrawable(res),
                lightTheme ? Color.BLACK : Color.WHITE);
    }

    public void reset(){
        mActualColor = color;
        updateColor();
    }
    public void updateColor(int color){
        mActualColor = color;
        updateColor();
    }
    public void updateColor(String color){
        updateColor(Color.parseColor(color));
    }

    public void updateColor(){
        MarqueeToolbar t = mRadioActivity.findViewById(R.id.toolbar);

        lightTheme = !Utils.isColorDark(mActualColor);
        int fgC = lightTheme ? Color.BLACK : Color.WHITE;

        // Status bar color
        Window window = mRadioActivity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(mActualColor);
        t.setSystemUiVisibility(lightTheme ? View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR : View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        // Action bar background color
        {
            t.setBackgroundColor(mActualColor);
            t.setTitleTextColor(fgC);
            t.setSubtitleTextColor(fgC);
        }

        // Overflow icon
        {
            Drawable drawable = t.getOverflowIcon();
            if(drawable != null) {
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable.mutate(), fgC);
                t.setOverflowIcon(drawable);
            }
        }
        // menu items
        {
            MenuItem item;
            Drawable drawable;

            for (int i = 0; i < t.getMenu().size(); i++){
                item = t.getMenu().getItem(i);
                drawable = item.getIcon();
                if (drawable == null)
                    continue;
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable.mutate(), fgC);
                item.setIcon(drawable);
            }
        }

    }

}
