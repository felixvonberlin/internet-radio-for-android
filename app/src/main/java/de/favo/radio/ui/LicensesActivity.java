package de.favo.radio.ui;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import de.favo.radio.R;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class LicensesActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WebView webView = new WebView(this);
        webView.loadUrl("file:///android_asset/index.html?lang="+getString(R.string.app_language));
        setContentView(webView);
    }
}
