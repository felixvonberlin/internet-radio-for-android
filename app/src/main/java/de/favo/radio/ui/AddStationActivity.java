package de.favo.radio.ui;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.ColorInt;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.pes.androidmaterialcolorpickerdialog.ColorPicker;
import com.pes.androidmaterialcolorpickerdialog.ColorPickerCallback;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;
import java.util.Vector;

import de.favo.radio.R;
import de.favo.radio.tools.Utils;
import de.favo.radio.tools.radio.RadioStation;
import de.favo.radio.tools.radio.RadioStationManager;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class AddStationActivity extends AppCompatActivity {

    public static final String[] SUPPORTED_PLAYLISTS = {".m3u",".pls"};

    private RadioStationManager mRadioStationManagement;

    private boolean isEditing;
    private RadioStation mRadioStationToEdit;
    private CircleView color;
    private EditText mNameTextEdit, mUrlTextEdit;
    private ImageButton mPasteImageButton;
    private ClipboardManager mClipboardManager;
    private AsyncPlaylistDownloader asyncPlaylistDownloader;
    private ProgressDialog loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_station);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(Utils.colorizeDrawable(getDrawable(R.drawable.icon_close),Color.BLACK));
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        isEditing = getIntent().getBooleanExtra("isEditing",false);

        color = findViewById(R.id.view_add_color);
        mNameTextEdit = findViewById(R.id.add_station_activity_station_name);
        mUrlTextEdit = findViewById(R.id.add_station_activity_station_url);
        mPasteImageButton = findViewById(R.id.add_station_activity_paste_button);

        Thread initiatorThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                mRadioStationManagement = new RadioStationManager(getApplicationContext());

                if (isEditing) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getSupportActionBar().setTitle(R.string.title_activity_edit_station);
                            setTitle(R.string.title_activity_edit_station);
                        }
                    });

                    mRadioStationToEdit = mRadioStationManagement
                            .findRadioStationByName(getIntent().getStringExtra("mNameTextEdit"));

                    color.setColor(Color.parseColor(mRadioStationToEdit.getColor()));

                    mNameTextEdit.setText(mRadioStationToEdit.getName());
                    mUrlTextEdit.setText(mRadioStationToEdit.getUrl());
                } else {
                    color.setColor(Color.rgb(48, 79, 254));
                    if (Intent.ACTION_VIEW.equals(getIntent().getAction()) && getIntent().getData() != null){
                        mUrlTextEdit.setText(getIntent().getData().toString());
                    }
                }

                color.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ColorPicker cp = new ColorPicker(AddStationActivity.this, Color.red(color.getColor()), Color.green(color.getColor()), Color.blue(color.getColor()));
                        cp.setCallback(new ColorPickerCallback() {
                            @Override
                            public void onColorChosen(@ColorInt int color) {
                                AddStationActivity.this.color.setColor(color);
                                updateColors(color);
                                cp.dismiss();
                            }});
                        cp.show();
                    }
                });

                mClipboardManager = (ClipboardManager) getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                // If the mClipboardManager doesn't contain data, disable the mPasteImageButton menu item.
                // If it does contain data, decide if you can handle the data.
                assert mClipboardManager != null;
                if (!(mClipboardManager.hasPrimaryClip())) {
                    mPasteImageButton.setEnabled(false);
                } else if (!(mClipboardManager.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {
                    // This disables the mPasteImageButton menu item, since the mClipboardManager has data but it is not plain text
                    mPasteImageButton.setEnabled(false);
                } else {
                    // This enables the mPasteImageButton menu item, since the mClipboardManager contains plain text.
                    mPasteImageButton.setEnabled(true);
                }

                mPasteImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ClipData.Item item = mClipboardManager.getPrimaryClip().getItemAt(0);
                        if (item.getText() != null) {
                            mUrlTextEdit.setText(item.getText());
                        } else {
                            Uri pasteUri = item.getUri();
                            if (pasteUri != null)
                                mUrlTextEdit.setText(item.getText());
                        }
                    }
                });
                //updateColors(color.getColor());
            }
        });
        initiatorThread.start();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        updateColors(color.getColor());
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("color",color.getColor());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        color.setColor(savedInstanceState.getInt("color",Color.BLUE));
        updateColors(color.getColor());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_save) {
            if (mNameTextEdit.getText().toString().trim().isEmpty())
                shakeEditText(mNameTextEdit);
            else if (mRadioStationManagement.isNameAlreadyExisting(mNameTextEdit.getText().toString()) && !isEditing){
                showDialog(R.string.text_station_already_exists,R.string.text_station_already_exists_text);
            }
            else if (mUrlTextEdit.getText().toString().trim().isEmpty())
                shakeEditText(mUrlTextEdit);
            else if (!(mUrlTextEdit.getText().toString().startsWith("http://") || mUrlTextEdit.getText().toString().startsWith("https://")))
                showDialog(R.string.text_error_wrong_url,R.string.text_error_wrong_url_text);
            else{
                //All things seems to be okay
                if (!Utils.contains(SUPPORTED_PLAYLISTS,
                                Utils.getFileEnding(mUrlTextEdit.getText().toString()))) {
                    saveNewStation(mNameTextEdit.getText().toString(), mUrlTextEdit.getText().toString(), color.getColor());
                    finish();
                }else{
                    loader = new ProgressDialog(this);
                    loader.setIndeterminate(true);
                    loader.setMessage(getString(R.string.text_adding_Station_checking_url));
                    loader.setTitle(R.string.text_adding_station);
                    loader.setCanceledOnTouchOutside(false);
                    loader.setCancelable(false);
                    loader.show();

                    asyncPlaylistDownloader = new AsyncPlaylistDownloader(new AsyncPlaylistDownloader.Callback() {
                        @Override
                        public void onFinished(final Vector<String> urls) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (loader != null)
                                        loader.cancel();

                                    if (urls.size() == 1){
                                        saveNewStation(mNameTextEdit.getText().toString(),urls.get(0),
                                                color.getColor());
                                        finish();
                                    } else
                                     saveWithPlaylistURLS(urls);
                                }
                            });
                        }

                        @Override
                        public void onError() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loader.cancel();
                                    showDialog(R.string.text_something_went_wrong,R.string.text_something_went_wrong_help);
                                }
                            });
                        }
                    });
                    asyncPlaylistDownloader.execute(mUrlTextEdit.getText().toString());
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveNewStation(String name, String url, int color){
        if (isEditing){
            int pos = mRadioStationManagement.findRadioStationPositionByName(mRadioStationToEdit.getName());
            mRadioStationManagement.deleteStationByPosition(pos);
            mRadioStationManagement.addStation(new RadioStation(name, url,
                    String.format("#%06X", 0xFFFFFF & color)),pos);
        }else {
            mRadioStationManagement.addStation(new RadioStation(name, url,
                    String.format("#%06X", 0xFFFFFF & color)));
        }
    }

    private void saveWithPlaylistURLS(Vector<String> urls){
        final String[] urlS = urls.toArray(new String[urls.size()]);
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setTitle(R.string.text_here_some_urls);
        alt_bld.setSingleChoiceItems(urlS, -1, new DialogInterface
                .OnClickListener() {
            public void onClick(DialogInterface dialog, final int item) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        saveNewStation(mNameTextEdit.getText().toString(),urlS[item],color.getColor());
                    }
                });
                dialog.dismiss();// dismiss the alertbox after chose option
                finish();

            }
        });
        AlertDialog alert = alt_bld.create();
        alert.show();

    }

    private void showDialog(int title, int text){
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(R.string.action_got_it, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                })
                .create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(final DialogInterface dialog) {
                        Button positiveButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                        positiveButton.setTextColor(color.getColor());
                        positiveButton.invalidate();
            }
        });
        alertDialog.show();

    }

    private void shakeEditText(final EditText t){
        final Vibrator vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake_animation);
        t.startAnimation(shake);
        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (vibrator != null && vibrator.hasVibrator())
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vibrator.vibrate(VibrationEffect.createOneShot(1000,255));
                    } else {
                        vibrator.vibrate(1000);
                    }
                t.getBackground().mutate().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                assert vibrator != null;
                vibrator.cancel();
                t.getBackground().mutate().setColorFilter(color.getColor(), PorterDuff.Mode.SRC_ATOP);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
    }

    private void updateColors(int bgC){
        int fgC = Utils.isColorDark(bgC) ? Color.WHITE : Color.BLACK;
        Toolbar t = findViewById(R.id.toolbar);

        // Status bar color
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(bgC);
        t.setSystemUiVisibility( fgC == Color.BLACK ? View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR :
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE);



        // Action bar background color
        {
            t.setBackgroundColor(bgC);
            t.setTitleTextColor(fgC);
            t.setSubtitleTextColor(fgC);
            t.setNavigationIcon(Utils.colorizeDrawable(getDrawable(R.drawable.icon_close),fgC));
        }

        // menu items
        {
            MenuItem item;
            Drawable drawable;

            for (int i = 0; i < t.getMenu().size(); i++){
                item = t.getMenu().getItem(i);
                drawable = item.getIcon();
                if (drawable == null)
                    continue;
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable.mutate(), fgC);
                item.setIcon(drawable);
            }
        }
        {
            mNameTextEdit.getBackground().mutate().setColorFilter(bgC, PorterDuff.Mode.SRC_ATOP);
            mUrlTextEdit.getBackground().mutate().setColorFilter(bgC, PorterDuff.Mode.SRC_ATOP);
        }
    }
    private static class AsyncPlaylistDownloader extends AsyncTask<String,Integer, Vector<String>>{

        private Callback callback;

        AsyncPlaylistDownloader(Callback callback){
            this.callback = callback;
        }

        @Override
        protected Vector<String> doInBackground(String... strings) {
            try {
                BufferedReader in;
                Vector<String> urls = new Vector<>(3);
                Vector<String> content = new Vector<>(10);
                URL url = new URL(strings[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String str;
                while ((str = in.readLine()) != null) {
                    content.add(str);
                }
                in.close();

                if (strings[0].endsWith("m3u")) {
                    urls.addAll(content);
                } else {
                    int pos = Utils.findLineWhichStartsWithIgnoreCase(content,"numberofentries=");
                    if (pos != -1){
                        int count = Integer.parseInt(content.get(pos).substring(16)), l;
                        for (int i = 1; i <= count; i++) {
                            l = Utils.findLineWhichStartsWithIgnoreCase(content,"file"+i);
                            urls.add(content.get(l).substring(("file"+i).length()+1));
                        }
                    }
                }

                {
                    //check for double URLS
                    if (urls.size() > 1){
                        boolean isMultiply = false;
                        for (int i = 0; i < urls.size(); i++) {
                            for (int j = 0; j < i; j++) {
                                isMultiply = isMultiply || urls.get(i).equals(urls.get(j));
                            }
                        }
                        if (isMultiply){
                            String s = urls.get(0);
                            urls.clear();
                            urls.add(s);
                        }
                    }
                }
                callback.onFinished(urls);
                return urls;

            } catch (Exception e) {
                callback.onError();
                e.printStackTrace();
            }
            return null;
        }
        interface Callback{
            void onFinished(Vector<String> urls);
            void onError();
        }
    }
}
