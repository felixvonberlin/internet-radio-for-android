package de.favo.radio.ui;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import de.favo.radio.R;
import de.favo.radio.tools.radio.RadioStation;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class RadioAdapter extends RecyclerView.Adapter<RadioAdapter.ViewHolder> {
    private ArrayList<RadioStation> mDataSet;
    private Activity mActivity;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout layout;

        public ViewHolder(LinearLayout v) {
            super(v);
            layout = v;
        }

        void selectItem(RadioStation item) {
        }

        void update(final RadioStation value){
            ((TextView)layout.findViewById(R.id.radio_list_item_name))
                    .setText(value.getName());
            ((TextView)layout.findViewById(R.id.radio_list_item_url))
                    .setText(value.getUrl());
            ((CircleView)layout
                    .findViewById(R.id.radio_list_item_color))
                    .setColor(Color.parseColor(value.getColor()));
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    ((RadioActivity) mActivity).onRadioStationLongPressed(value);
                    return true;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectItem(value);
                    ((RadioActivity) mActivity).onPlayRadioStation(value);
                }
            });
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RadioAdapter(Activity activity, ArrayList<RadioStation> myDataset) {
        mDataSet = myDataset;
        this.mActivity = activity;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public RadioAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                      int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.radio_list_item, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.update(mDataSet.get(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}


