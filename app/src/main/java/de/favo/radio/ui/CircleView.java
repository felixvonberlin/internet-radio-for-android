package de.favo.radio.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import de.favo.radio.R;
import de.favo.radio.tools.Utils;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class CircleView extends View {
    private int mExampleColor = Color.RED; // TODO: use a default from R.color...

    private Paint mPaint;
    private Paint mDarkPaint;

    public CircleView(Context context) {
        super(context);
        init(null, 0);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.CircleView, defStyle, 0);

        mExampleColor = a.getColor(
                R.styleable.CircleView_exampleColor,
                mExampleColor);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDarkPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        setColor(mExampleColor);

        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mDarkPaint.setStyle(Paint.Style.STROKE);

        mDarkPaint.setStrokeWidth(5);
        a.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(
                getWidth() / 2,
                getHeight() / 2,
                (getWidth()-10) / 2,
                mPaint);

        canvas.drawCircle(
                        getWidth() / 2,
                getHeight() / 2,
                (getWidth()-10) / 2,
                mDarkPaint);
    }

    /**
     * Gets the example string attribute value.
     * <p>
     * <p>
     * /**
     * Gets the example color attribute value.
     *
     * @return The example color attribute value.
     */
    public int getColor() {
        return mExampleColor;
    }

    /**
     * Sets the view's example color attribute value. In the example view, this color
     * is the font color.
     *
     * @param exampleColor The example color attribute value to use.
     */
    public void setColor(int exampleColor) {
        mExampleColor = exampleColor;
        mPaint.setColor(mExampleColor);
        mDarkPaint.setColor(Utils.darker(exampleColor,0.7f));
        invalidate();
    }
}
