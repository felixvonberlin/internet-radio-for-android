[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![Android](https://img.shields.io/badge/designed%20for-Android-green.svg?logo=android&color=a4c639)](https://www.android.com)
[![Java](https://img.shields.io/badge/using-Java-blue.svg?logo=java&color=007396)](http://www.oracle.com/technetwork/java/index.html)

# Internet Radio for Android

## Where can I get it?
You could download it here:
[play.google.com/store/apps/details?id=de.favo.radio](https://play.google.com/store/apps/details?id=de.favo.radio)

or compile it your self.

## Can I support this project?
I would like you to support me.
For example, for adding new features or designing a beautiful app icon.

Or translating into other languages than german ;-)

## What's about iOS?
There is also an app developed by me; but it is not published so you need to build it for your own.
